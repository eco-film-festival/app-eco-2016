angular.module('starter', ['ionic', 'starter.controllers', 'starter.services'])

.run(function($ionicPlatform ,$rootScope , $state) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
  /*
  $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
    if( toState.name != 'app.welcome' && !$rootScope.editionActive){
      event.preventDefault();
      $state.go('app.welcome');
    }
  });
  */
})

.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider
  // ------------------------------------
  // ------------------------------------
  .state('app', {
    url:'/app',
    abstract: true,
    templateUrl : '/templates/app.html',
  })
  // ------------------------------------
  // ------------------------------------
  .state('app.edition',{
    url:'/:year',
    abstract: true,
    views:{
      'section-app' :{
        templateUrl: 'templates/section-edition.html',
      }
    }
  })
  .state('app.edition.festival' , {
    url:'/festival',
    views:
      {
        'tab-festival' : {
          templateUrl: 'templates/tab/tab-festival.html',
          controller : function($scope , $stateParams){
            $scope.$emit('year' , $stateParams.year);
            $scope.$emit('activeRightMenu' , false);
          }
        }
      }
  })
  .state('app.edition.sel-of' , {
    url:'/sel-of',
    views:
      {
        'tab-sel-of' : {
          templateUrl: 'templates/tab/tab-sel-of.html',
          controller : function($scope , $stateParams){
            $scope.$emit('year' , $stateParams.year);
            $scope.$emit('activeRightMenu' , true);
          }
        },
        'right-menu@app' : {
          templateUrl : '/templates/right/right-sel-of.html',
        }
      }
  })
  .state('app.edition.jury' , {
    url:'/jury',
    views:
      {
        'tab-jury' : {
          templateUrl: 'templates/tab/tab-jury.html',
          controller : function($scope , $stateParams){
            $scope.$emit('year' , $stateParams.year);
            $scope.$emit('activeRightMenu' , false);
          }
        }
      }
  })
  .state('app.edition.schedule' , {
    url:'/schedule',
    views:
      {
        'tab-schedule' : {
          templateUrl: 'templates/tab/tab-schedule.html',
          controller:'ScheduleCtrl' ,
        },
        'right-menu@app' : {
          templateUrl : '/templates/right/right-schedule.html'
        }
      }
  })
  .state('app.edition.winners' , {
    url:'/winners',
    views:
      {
        'tab-winners' : {
          templateUrl: 'templates/tab/tab-winners.html',
          controller : function($scope , $stateParams){
            $scope.$emit('year' , $stateParams.year);
            $scope.$emit('activeRightMenu' , true);
          }
        },
        'right-menu@app' : {
          templateUrl : '/templates/right/right-winners.html'
        }
      }
  })
  // ------------------------------------
  // ------------------------------------
  .state('app.welcome',{
    url:'/welcome',
    views:{
      'section-app' :{
        templateUrl: 'templates/section-welcome.html',
        controller : 'WelcomeSectionCtrl'
      }
    }
  })
  .state('app.sponsor',{
    url:'/sponsor',
    views:{
      'section-app' :{
        templateUrl: 'templates/section-sponsor.html',
        //controller : 'WelcomeSectionCtrl'
      }
    }
  })
  .state('app.feed',{
    url:'/feed',
    views:{
      'section-app' :{
        templateUrl: 'templates/section-feed.html',
        //controller : 'WelcomeSectionCtrl'
      }
    }
  })
  // ------------------------------------
  // ------------------------------------
  ;
  $urlRouterProvider.otherwise('/app/welcome');

});
