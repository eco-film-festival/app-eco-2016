angular.module('starter.controllers', [])
// ------------------------------------
// ------------------------------------
// ------------------------------------
// ------------------------------------
.controller('AppCtrl' , function ($scope , $state , $ionicHistory , $rootScope , $q , EcoService) {
  $scope.loading = true;
  EcoService.getListEditions().then(function(response){
    $scope.listEditions = response.data;
    $scope.loading = false;
  });
  // --------------------------------
  // --------------------------------
  // --------------------------------
  $scope.goEdition = function(item){
    $scope.activeEdition = item;
    $state.go('app.edition.festival', {year: item.year} , false );
    $ionicHistory.clearHistory();
  };
  // --------------------------------
  // --------------------------------
  // --------------------------------
  $scope.loaginEdition = false;
  $scope.$on('year', function(e , year) {
    //$rootScope.editionActive = true;
    if(!$scope.loaginEdition){
      $scope.loaginEdition = true;
      $scope.loading = true;
      var qEdition =  EcoService.getEditionByEdition(year);
      $q.all(
        qEdition.then(function(response){
          $scope.data = response.data;
        })
      ).then(function(){
        $scope.loaginEdition = false;
        $scope.loading = false;
      })
      console.info('app.edition' , year);
    }

  });
  // --------------------------------
  // --------------------------------
  // --------------------------------
  $scope.rightMenuActive = false;
  $scope.$on('activeRightMenu', function(e , active) {
    $scope.rightMenuActive = active;
  })
  console.info('AppCtrl');
})
// ------------------------------------
// ------------------------------------
.controller('EditionSectionCtrl' , function($scope , $stateParams){
  $scope.$emit('year' , $stateParams.year);
})
// ------------------------------------
// ------------------------------------
.controller('WelcomeSectionCtrl' , function($scope , $q , $rootScope , EcoService){
  $rootScope.rightMenuActive = false;
  $scope.loading = true;
  var qSponsorHome = EcoService.getSponsorHome();
  $q.all(
    qSponsorHome.then(function(response){
      $scope.listSponsorHome = response.data;
    })
  ).then(function(){
    $scope.loading = false;
  });
  console.info('WelcomeCtrl');
})
// ------------------------------------
// ------------------------------------
.controller('ScheduleCtrl' , function($scope , $q , EcoService){
  console.info('ScheduleCtrl');
  $scope.loading = true;
  var qSponsorHome = EcoService.getSchedule();
  $q.all(
    qSponsorHome.then(function(response){
      $scope.data = response.data;
    })
  ).then(function(){
    $scope.loading = false;
  });
})





;
