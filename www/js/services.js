angular.module('starter.services', [])
.factory('EcoService' , function($http , $q){


  // http://ecofilmfestival.info/api/PublicService/App
  // http://ecofilmfestival.info/api/PublicService/Report
  // http://ecofilmfestival.info/api/PublicService/Schedule
  // http://ecofilmfestival.info/api/PublicService/Event/6
  // http://ecofilmfestival.info/api/PublicService/Projection/6
  // http://ecofilmfestival.info/api/PublicService/ShortFilm/987
  // http://ecofilmfestival.info/api/PublicService/OfficialSelection
  // http://ecofilmfestival.info/api/PublicService/PreSeleccion
  // http://ecofilmfestival.info/api/PublicService/Jury
  // http://ecofilmfestival.info/api/PublicService/Prensa
  // http://ecofilmfestival.info/api/PublicService/Feed
  // http://ecofilmfestival.info/api/PublicService/Winners
  // http://ecofilmfestival.info/api/PublicService/Sponsor
  // http://ecofilmfestival.info/api/PublicService/SponsorHome
  // http://ecofilmfestival.info/api/PublicService/Edition
  // http://ecofilmfestival.info/api/PublicService/EditionByEdition/2015

    var _host = 'http://ecofilmfestival.info';
    var _service = '/api/PublicService/';
    var _hostService = _host + _service;
    var _listEdition = [];
    return {
      getEditionByEdition : function(year) {
        var _q = $q.defer();
        if(_listEdition[year] === undefined) {
          $http.get(_hostService + 'EditionByEdition/' + year).then(
            function(response){
              _listEdition[year]  = response.data;
              _q.resolve({data: _listEdition[year]});
            } ,
            function(response){ _q.reject(response);}
          );
        } else {
          _q.resolve({data: _listEdition[year]});
        }
        return _q.promise;
      },
      getListEditions : function () {
        var d = $q.defer();
        d.resolve(
          {data:[
            {
              edition : 'Nombre de la edicion 2016' ,
              year : 2016 ,
              festival : true ,
              selOf : true ,
              jury : true ,
              schedule : true ,
              winners : true ,
              actual : true ,
            } ,
            {
              edition : 'Nombre de la edicion 2015' ,
              year : 2015 ,
              festival : true ,
              selOf : true ,
              jury : true ,
              schedule : false ,
              winners : true ,
              actual : false ,
            } ,
            {
              edition : 'Nombre de la edicion 2014' ,
              year : 2014 ,
              festival : true ,
              selOf : true ,
              jury : true ,
              schedule : false ,
              winners : true ,
              actual : false ,
            } ,
            {
              edition : 'Nombre de la edicion 2013' ,
              year : 2013 ,
              festival : true ,
              selOf : true ,
              jury : true ,
              schedule : false ,
              winners : true ,
              actual : false ,
            } ,
            {
              edition : 'Nombre de la edicion 2012' ,
              year : 2012 ,
              festival : true ,
              selOf : true ,
              jury : true ,
              schedule : false ,
              winners : true ,
              actual : false ,
            } ,
            {
              edition : 'Nombre de la edicion 2011' ,
              year : 2011 ,
              festival : true ,
              selOf : false ,
              jury : true ,
              schedule : false ,
              winners : true ,
              actual : false ,
            } ,
          ]}
        );
        return d.promise;
      } ,
      getApp : function(){
        return $http.get(_hostService + 'App');
      },
      getReport : function(){
        return $http.get(_hostService + 'Report');
      },
      getSchedule : function() {
        return $http.get(_hostService + 'Schedule');
      },
      getEvent : function(id) {
        return $http.get(_hostService + 'Event/' + id);
      },
      getProjection : function (id) {
        return $http.get(_hostService + 'Projection/' + id);
      },
      getShortFilm : function (id){
        return $http.get(_hostService + 'ShortFilm/' + id);
      },
      getOfficialSelection : function () {
        return $http.get(_hostService + 'OfficialSelection');
      },
      getPreSeleccion : function () {
        return $http.get(_hostService + 'PreSeleccion');
      },
      getJury : function() {
        return $http.get(_hostService + 'Jury');
      },
      getPrensa : function() {
        return $http.get(_hostService + 'Prensa');
      },
      getFeed : function() {
        return $http.get(_hostService + 'Feed');
      },
      getWinners : function() {
        return $http.get(_hostService + 'Winners');
      },
      getSponsor : function() {
        return $http.get(_hostService + 'Sponsor');
      },
      getSponsorHome : function(){
        return $http.get(_hostService + 'SponsorHome');
      },
      getEdition : function() {
        return $http.get(_hostService + 'Edition');
      },

    }
})
.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [{
    id: 0,
    name: 'Ben Sparrow',
    lastText: 'You on your way?',
    face: 'img/ben.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    lastText: 'Hey, it\'s me',
    face: 'img/max.png'
  }, {
    id: 2,
    name: 'Adam Bradleyson',
    lastText: 'I should buy a boat',
    face: 'img/adam.jpg'
  }, {
    id: 3,
    name: 'Perry Governor',
    lastText: 'Look at my mukluks!',
    face: 'img/perry.png'
  }, {
    id: 4,
    name: 'Mike Harrington',
    lastText: 'This is wicked good ice cream.',
    face: 'img/mike.png'
  }];

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
});
